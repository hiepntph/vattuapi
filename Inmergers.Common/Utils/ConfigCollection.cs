﻿
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Inmergers.Common
{
    public class ConfigCollection
    {
        private readonly IConfigurationRoot configuration;
        public static ConfigCollection Instance { get; } = new ConfigCollection();

        protected ConfigCollection()
        {
            configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                                                             .AddJsonFile("appsettings.json", optional: true, reloadOnChange: false)
                                                             .AddJsonFile($"appsettings.Production.json",
                                                             optional: true, reloadOnChange: false)
                                                             .Build();
        }

        public IConfigurationRoot GetConfiguration()
        {
            return configuration;
        }
    }
}