﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using Inmergers.Common.Utils;
using Microsoft.IdentityModel.Tokens;

namespace Inmergers.Common
{
    public class Helper
    {
        public static ActionResult TransformData(Response data)
        {
            var result = new ObjectResult(data) { StatusCode = (int)data.Code };
            return result;
        }
        /// <summary>
        /// Get user info in token and headder
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static RequestUser GetRequestInfo(HttpRequest request)
        {
            try
            {
                var result = new RequestUser
                {
                    UserId = Guid.Empty,
                    UserName = "",
                    ApplicationId = Guid.Empty
                };

                // Language
                request.Headers.TryGetValue("x-language", out StringValues language);
                if (string.IsNullOrEmpty(language))
                {
                    language = InmergersConstants.LanguageConstants.Default; result.Language = language;
                }
                else
                {
                    string lang = language;
                    lang = lang.ToLower();
                    result.Language = lang;
                }

                // Currency
                request.Headers.TryGetValue("x-currency", out StringValues currency);
                if (string.IsNullOrEmpty(currency))
                {
                    currency = "USD";
                    result.Currency = currency;
                }
                else
                {
                    result.Currency = ((string)currency).ToUpper();
                }

                request.Headers.TryGetValue("X-Permission", out StringValues currentToken);
                if (string.IsNullOrEmpty(currentToken))
                {
                    var token = request.Headers["Authorization"].ToString();
                    if (!string.IsNullOrEmpty(token))
                    {
                        var tokenString = token.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries);
                        if (tokenString.Length > 1)
                        {
                            currentToken = tokenString[1]?.Trim();
                        }
                    }
                }

                if (!string.IsNullOrEmpty(currentToken))
                {
                    string secret = UtilsGetConfig.GetConfig("Authentication:Jwt:Key");
                    var key = Encoding.ASCII.GetBytes(secret);
                    var handler = new JwtSecurityTokenHandler();
                    var validations = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidIssuer = UtilsGetConfig.GetConfig("Authentication:Jwt:Issuer"),
                        ValidAudience = UtilsGetConfig.GetConfig("Authentication:Jwt:Issuer"),
                    };
                    var currentUser = handler.ValidateToken(currentToken, validations, out var tokenSecure);

                    //UserId
                    if (currentUser.HasClaim(c => c.Type == InmergersConstants.ClaimConstants.USER_ID))
                    {
                        var userId = currentUser.Claims.FirstOrDefault(c => c.Type == InmergersConstants.ClaimConstants.USER_ID)?.Value;
                        if (!string.IsNullOrEmpty(userId) && UtilsGetConfig.IsGuid(userId))
                        {
                            result.UserId = new Guid(userId);
                        }
                    }
                    else
                    {
                        request.Headers.TryGetValue("X-UserId", out StringValues userId);
                        if (!string.IsNullOrEmpty(userId) && UtilsGetConfig.IsGuid(userId))
                        {
                            result.UserId = new Guid(userId);
                        }
                    }

                    //UserName
                    if (currentUser.HasClaim(c => c.Type == InmergersConstants.ClaimConstants.USER_NAME))
                    {
                        var userName = currentUser.Claims.FirstOrDefault(c => c.Type == InmergersConstants.ClaimConstants.USER_NAME)?.Value;
                        if (!string.IsNullOrEmpty(userName))
                        {
                            result.UserName = userName;
                        }
                    }

                    //LoginName
                    if (currentUser.HasClaim(c => c.Type == InmergersConstants.ClaimConstants.FULL_NAME))
                    {
                        var fullName = currentUser.Claims.FirstOrDefault(c => c.Type == InmergersConstants.ClaimConstants.FULL_NAME)?.Value;
                        if (!string.IsNullOrEmpty(fullName))
                        {
                            result.UserName = fullName;
                        }
                    }

                    //PhoneNumber
                    if (currentUser.HasClaim(c => c.Type == InmergersConstants.ClaimConstants.PHONE))
                    {
                        var phoneNumber = currentUser.Claims.FirstOrDefault(c => c.Type == InmergersConstants.ClaimConstants.PHONE)?.Value;
                        if (!string.IsNullOrEmpty(phoneNumber))
                        {
                            result.PhoneNumber = phoneNumber;
                        }
                    }
                    else
                    {
                        request.Headers.TryGetValue("X-phone", out StringValues phoneNumber);
                        if (!string.IsNullOrEmpty(phoneNumber))
                        {
                            result.PhoneNumber = phoneNumber;
                        }
                    }

                    // //AppId
                    // if (currentUser.HasClaim(c => c.Type == ClaimConstants.APP_ID))
                    // {
                    //     var appId = currentUser.Claims.FirstOrDefault(c => c.Type == ClaimConstants.APP_ID)?.Value;
                    //     if (!string.IsNullOrEmpty(appId) && Utils.IsGuid(appId))
                    //     {
                    //         result.ApplicationId = new Guid(appId);
                    //     }
                    // }
                    // else
                    // {
                    request.Headers.TryGetValue("X-ApplicationId", out StringValues applicationId);
                    if (!string.IsNullOrEmpty(applicationId) && UtilsGetConfig.IsGuid(applicationId))
                    {
                        result.ApplicationId = new Guid(applicationId);
                    }
                    else
                    {
                        if (currentUser.HasClaim(c => c.Type == InmergersConstants.ClaimConstants.APP_ID))
                        {
                            var appId = currentUser.Claims.FirstOrDefault(c => c.Type == InmergersConstants.ClaimConstants.APP_ID)?.Value;
                            if (!string.IsNullOrEmpty(appId) && UtilsGetConfig.IsGuid(appId))
                            {
                                result.ApplicationId = new Guid(appId);
                            }
                        }
                    }
                    // }

                    //ListRoles
                    var listRoles = currentUser.Claims.FirstOrDefault(c => c.Type == InmergersConstants.ClaimConstants.ROLES)?.Value;
                    if (!string.IsNullOrEmpty(listRoles))
                    {
                        result.ListRoles = JsonConvert.DeserializeObject<List<string>>(listRoles);
                    }
                    //ListRights
                    var listRights = currentUser.Claims.FirstOrDefault(c => c.Type == InmergersConstants.ClaimConstants.RIGHTS)?.Value;
                    if (!string.IsNullOrEmpty(listRights))
                    {
                        result.ListRights = JsonConvert.DeserializeObject<List<string>>(listRights);
                    }
                }
                else
                {
                    request.Headers.TryGetValue("X-ApplicationId", out StringValues applicationId);
                    if (!string.IsNullOrEmpty(applicationId) && UtilsGetConfig.IsGuid(applicationId))
                    {
                        result.ApplicationId = new Guid(applicationId);
                    }
                    request.Headers.TryGetValue("X-UserId", out StringValues userId);
                    if (!string.IsNullOrEmpty(userId) && UtilsGetConfig.IsGuid(userId))
                    {
                        result.UserId = new Guid(userId);
                    }
                    request.Headers.TryGetValue("X-UserName", out StringValues userName);
                    if (!string.IsNullOrEmpty(userName))
                    {
                        result.UserName = userName;
                    }
                }

                result.isAdmin = false;
                result.isStaff = false;
                result.isAdmin = result.ListRoles != null && result.ListRoles.Contains(RoleConstants.AdministratorCode);
                result.isStaff = result.ListRoles != null && result.ListRoles.Contains(RoleConstants.StaffCode);
                // Level
                result.Level = UtilsGetConfig.GetUserLevelFromRoles(result.ListRoles);
                return result;
            }
            catch (Exception exx)
            {
                Log.Error(exx, string.Empty);
                throw;
            }
        }

        public class RequestUser
        {
            public Guid UserId { get; set; }
            public string UserName { get; set; }
            public string FullName { get; set; }
            public string PhoneNumber { get; set; }
            public Guid ApplicationId { get; set; }
            public List<string> ListApps { get; set; }
            public List<string> ListRoles { get; set; }
            public List<string> ListRights { get; set; }
            public bool isAdmin { get; set; }
            public bool isStaff { get; set; }
            public int Level { get; set; }
            public string Language { get; set; }
            public string Currency { get; set; }
        }
    }

}
