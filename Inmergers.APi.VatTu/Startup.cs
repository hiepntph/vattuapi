﻿using Inmergers.APi.VatTu.Background_Task;
using Inmergers.APi.VatTu.FakeData;
using Inmergers.Business.Service.BackGroundTask;
using Inmergers.Business.Service.BoPhan;
using Inmergers.Business.Service.MatHang;
using Inmergers.Business.Service.NhanVien;
using Inmergers.Business.Service.PhieuDangKyMuaHang;
using Inmergers.Business.Service.ThongKe;
using Inmergers.Business.Service.TimKiem;
using Inmergers.Business.Service.TongSap;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inmergers.APi.VatTu
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHostedService<CapNhatPhongBanDangKiNhieuSanPhamNhat>();
            #region Swagger
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "VatTu API",
                    Description = "An ASP.NET Core Web API for managing ToDo items",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Example Contact",
                        Url = new Uri("https://example.com/contact")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Example License",
                        Url = new Uri("https://example.com/license")
                    }
                });

                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                    }
                });
            });
            #endregion
            services.AddResponseCaching();
            services.AddControllers(setupAction =>
            {
                setupAction.ReturnHttpNotAcceptable = true;
                setupAction.CacheProfiles.Add("90SecondsCacheProfile", new CacheProfile { Duration = 90 });//
            }).AddXmlDataContractSerializerFormatters().AddNewtonsoftJson(
                setupAction =>
                {
                    setupAction.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                }
            );
            services.AddControllers();

            services.AddAutoMapper(typeof(Startup));
            services.AddScoped<IBoPhanServices, BoPhanServices>();
            services.AddScoped<IBackGroundCapNhat, BackGroundCapNhat>();
            services.AddScoped<IMatHangServices, MatHangServices>();
            services.AddScoped<IPhieuDangKyMuaHangServices, PhieuDangKyMuaHangServices>();
            services.AddScoped<INhanVienServices, NhanVienServices>();
            services.AddScoped<IThongKeServices, ThongkeServices>();
            services.AddScoped<ITongSapServices, TongSapServices>();
            services.AddScoped<ITimKiemServices, TimKiemServices>();
            services.AddScoped<Seedding>();

            #region addtokencu




            // services.Configure<AppSetting>(Configuration.GetSection("AppSettings"));
            // var secretKey = Configuration["AppSettings:SecretKey"];
            // var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            // //
            // services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(opt =>
            // {
            //     opt.TokenValidationParameters = new TokenValidationParameters
            //     {
            //         // cho phép bỏ qua việc kiểm tra issu và audience trong token
            //         ValidateIssuer = false,
            //         ValidateAudience = false,
            //
            //         // cho phép xác thực key signing cho token 
            //         ValidateIssuerSigningKey = true,
            //         // chỉ định key được sử dụng để xác thực token 
            //         IssuerSigningKey = new SymmetricSecurityKey(secretKeyBytes),
            //         //đặt thời gian skew về 0 cho phép xác thực token chính xác theo thời gian thực
            //         ClockSkew = TimeSpan.Zero
            //     };
            // });
            #endregion
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        ValidAudience = Configuration["Jwt:Audience"],
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };

                });
            services.AddHttpContextAccessor();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddDbContext<vattudbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("ketnoi"));
                //options.UseSqlServer(Configuration.GetConnectionString("ketnoi"));
            });

        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger(options =>
                {
                    options.SerializeAsV2 = true;
                });
                app.UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                    options.RoutePrefix = string.Empty;
                });
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async c =>
                    {
                        c.Response.StatusCode = 500;
                        await c.Response.WriteAsync("Something went horribly wrong, try again later");
                    });
                });
            }


            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

            });

        }
    }
}
