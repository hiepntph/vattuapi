﻿using AutoMapper;
using Inmergers.Business.Service.BoPhan;
using Inmergers.Business.Service.MatHang;
using Inmergers.Business.Service.NhanVien;
using Inmergers.Business.Service.PhieuDangKyMuaHang;
using Inmergers.Business.Service.TimKiem;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.Entity;

namespace Inmergers.APi.VatTu.AutoMapper
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<boPhan, BoPhanModels>().ReverseMap();
            CreateMap<MatHang, MatHangModels>().ReverseMap();
            CreateMap<NhanVien, NhanVienModel>().ReverseMap();
            CreateMap<TimKiem, TimKiemModels>().ReverseMap();
            #region phieudangKy
            CreateMap<PhieuDangKyMuaHang, PhieuDangKyMuaHangModels>().ReverseMap();
            CreateMap<PhieuDangKyMuaHang, PhieuDangKyCreateModel>().ReverseMap();

            CreateMap<ChiTietPhieuDangKyCreate, ChiTietPhieuMuaHang>();
            CreateMap<Pagination<PhieuDangKyMuaHang>, Pagination<PhieuDangKyMuaHangModels>>();
            CreateMap<PhieuDangKyCreateModel, PhieuDangKyMuaHang>().ForMember(dest => dest.ChiTietPhieuMuaHang, opt => opt.MapFrom(src => src.MatHangCreate));
            CreateMap<ChiTietPhieuMuaHang, ChiTietPhieuDangKyModel>();
            #endregion
        }
    }
}
