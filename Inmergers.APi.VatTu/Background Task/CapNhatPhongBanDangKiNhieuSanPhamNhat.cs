﻿using Inmergers.Business.Service.BackGroundTask;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Inmergers.APi.VatTu.Background_Task
{
    public class CapNhatPhongBanDangKiNhieuSanPhamNhat : BackgroundService
    {
        public IServiceProvider Services { get; }
        private readonly ILogger<CapNhatPhongBanDangKiNhieuSanPhamNhat> _logger;
        public int solan;

        public CapNhatPhongBanDangKiNhieuSanPhamNhat(ILogger<CapNhatPhongBanDangKiNhieuSanPhamNhat> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            Services = serviceProvider;

        }

        public async Task DoWorkAsync(CancellationToken cancellationToken)
        {
            Interlocked.Increment(ref solan);
            _logger.LogInformation($"So Lan Cap Nhat: {solan}");

            using (var scope = Services.CreateScope())
            {
                var scopedProcessingService =
                    scope.ServiceProvider
                        .GetRequiredService<IBackGroundCapNhat>();

                await scopedProcessingService.PhongBanDangKyNhieuSanPhamNhat(cancellationToken);
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Hosted Service is stopping.");

            await base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {

            await DoWorkAsync(cancellationToken);
        }


    }
}
