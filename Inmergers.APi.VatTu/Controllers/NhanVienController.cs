﻿using AutoMapper;
using Inmergers.Business.Hubs.Models;
using Inmergers.Business.Service.NhanVien;
using Inmergers.Common;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Inmergers.APi.VatTu.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/[controller]")]
    public class NhanVienController : ControllerBase
    {

        private readonly vattudbContext _vatuVattudbContext;
        private readonly IMapper _mapper;
        private readonly AppSetting _appSettings;
        private readonly INhanVienServices _inhanVienServices;

        public NhanVienController(vattudbContext vattudbContext, IMapper mapper, IOptionsMonitor<AppSetting> optionsMonitor, INhanVienServices inhaNhanVienServices)
        {
            _vatuVattudbContext = vattudbContext;
            _mapper = mapper;
            _appSettings = optionsMonitor.CurrentValue;
            _inhanVienServices = inhaNhanVienServices;
        }

        /// <summary>
        /// Thêm mới nhân viên
        /// </summary>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPost, Route("Creater")]
        public async Task<IActionResult> createNhanVien([FromQuery] CreateNhanvienAndUpdate model)
        {
            var result = await _inhanVienServices.createNhanVien(model);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy danh sách nhân viên
        /// </summary>
        /// <param name="model">dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("Get")]
        public async Task<IActionResult> getNhanVien([FromQuery] NhanVienQueryModel model)
        {
            var result = await _inhanVienServices.getNhanVien(model);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy danh sách nhân viên bằng Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("GetById/{Id}")]
        public async Task<IActionResult> getIdNhanVien(Guid Id)
        {
            var result = await _inhanVienServices.getIdNhanVien(Id);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Sửa thông tin nhân viên
        /// </summary>
        /// <param name="Id">Id bản ghi</param>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPut, Route("Update")]
        public async Task<IActionResult> updateNhanVien(Guid Id, [FromBody] CreateNhanvienAndUpdate model)
        {
            var resule = await _inhanVienServices.updateNhanVien(Id, model);
            return Helper.TransformData(resule);
        }
        /// <summary>
        /// XÓa nhân viên
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpDelete, Route("Delete/{Id}")]
        public async Task<IActionResult> deleteNhanVien(Guid Id)
        {
            var resule = await _inhanVienServices.deleteNhanVien(Id);
            return Helper.TransformData(resule);
        }
    }
}
