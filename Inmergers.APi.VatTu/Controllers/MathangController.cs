﻿using AutoMapper;
using Inmergers.Business.Service.MatHang;
using Inmergers.Common;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Inmergers.APi.VatTu.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/[controller]")]
    public class MathangController : ControllerBase
    {
        private readonly vattudbContext _vatuVattudbContext;
        private readonly IMapper _mapper;
        private readonly IMatHangServices _IMatHangServices;

        public MathangController(vattudbContext vattudbContext, IMapper mapper, IMatHangServices IMatHangServices)
        {
            _vatuVattudbContext = vattudbContext;
            _mapper = mapper;
            _IMatHangServices = IMatHangServices;
        }

        /// <summary>
        /// lấy danh sách mặt hàng
        /// </summary>
        /// <param name="matHang"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("Get")]
        public async Task<IActionResult> getMatHang([FromQuery] MatHangQueryModel matHang)
        {
            var matHangRepo = await _IMatHangServices.getMatHang(matHang);
            return Helper.TransformData(matHangRepo);
        }

        /// <summary>
        /// lấy danh sách mặt hàng bằng Id
        /// </summary>
        /// <param name="Id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("{Id}/GetById")]
        public async Task<IActionResult> getIdMatHang(Guid Id)
        {
            var result = await _IMatHangServices.getIdMatHang(Id);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Thêm mới mặt hàng
        /// </summary>
        /// <param name="matHang"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPost, Route("Create")]
        public async Task<IActionResult> createMatHang([FromQuery] MatHangCreateAndUpdateModels model)
        {
            var result = await _IMatHangServices.createMatHang(model);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Sửa thông tin mặt hàng
        /// </summary>
        /// <param name="Id">Id bản ghi</param>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPut, Route("Update")]
        public async Task<IActionResult> updateMatHang(Guid Id, [FromBody] MatHangCreateAndUpdateModels model)
        {
            var result = await _IMatHangServices.updateMatHang(Id, model);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Xóa mặt hàng
        /// </summary>
        /// <param name="Id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpDelete, Route("{Id}/Delete")]
        public async Task<IActionResult> deleteMatHang(Guid Id)
        {
            var result = await _IMatHangServices.deleteMatHang(Id);
            return Helper.TransformData(result);
        }
    }
}
