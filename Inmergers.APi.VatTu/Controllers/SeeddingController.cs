﻿using Inmergers.APi.VatTu.FakeData;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace Inmergers.APi.VatTu.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    public class SeeddingController
    {
        private Seedding _seedding;

        public SeeddingController(Seedding seedding)
        {
            this._seedding = seedding;
        }


        [HttpGet]
        [Route("Seedding")]
        public async Task<string> SeeddingData()
        {
            // await _seedding.SeeddingBoPhan();
            // await _seedding.SeeddingNhanVien();
            // await _seedding.SeeddingSanPham();
            // await _seedding.SeeddingPhieuDangKyMuahang();
            await _seedding.SeeddingTimKiem();
            return "Thành Công";
        }
    }
}
