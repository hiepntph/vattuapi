﻿using AutoMapper;
using Inmergers.Business.Service.PhieuDangKyMuaHang;
using Inmergers.Common;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Inmergers.APi.VatTu.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    public class PhieuDangKyMuaHangController : ControllerBase
    {
        private readonly vattudbContext _Context;
        private readonly IMapper _mapper;
        private readonly IPhieuDangKyMuaHangServices _IDangKyMuaHangServices;

        public PhieuDangKyMuaHangController(vattudbContext Context, IMapper mapper, IPhieuDangKyMuaHangServices IDangKyMuaHangServices)
        {
            _Context = Context;
            _mapper = mapper;
            _IDangKyMuaHangServices = IDangKyMuaHangServices;
        }

        /// <summary>
        /// Lấy danh sách phiếu đăng ký
        /// </summary>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("Get")]
        public async Task<IActionResult> getPhieuDangKyMuaHang([FromQuery] PhieuDangKyQueryModel model)
        {
            var boPhanRepo = await _IDangKyMuaHangServices.getPhieuDangKyMuaHangg(model);
            return Helper.TransformData(boPhanRepo);
        }

        /// <summary>
        /// Lấy danh sách phiếu đăng ký bằng Id
        /// </summary>
        /// <param name="Id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("{Id}/GetById")]
        public async Task<IActionResult> getIdBoPhan(Guid Id)
        {
            var result = await _IDangKyMuaHangServices.getIPhieuDangKyMuaHangg(Id);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Tạo phiếu đăng ký mua hàng
        /// </summary>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPost, Route("Create")]
        public async Task<IActionResult> createPhieuDangKyMuaHang(PhieuDangKyCreateModel model)
        {
            #region thêm khi đăng nhập
            // var identity = HttpContext.User.Identity as ClaimsIdentity;
            // if (identity != null)
            // {
            //     var id = identity.FindFirst("Id").Value;
            //     pdk.IdNhanVien = Guid.Parse(id);
            // }
            // var resule = await _IDangKyMuaHangServices.createPhieuDangKyMuaHang(pdk);
            // return Helper.TransformData(resule);
            #endregion
            var resule = await _IDangKyMuaHangServices.createPhieuDangKyMuaHang(model);
            return Helper.TransformData(resule);
        }

        /// <summary>
        /// Tạo phiếu đăng ký mua hàng khi login Nhân viên
        /// </summary>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPost, Route("CreateLogin")]
        public async Task<IActionResult> createPhieuDangKyMuaHang([FromBody] PhieuDangKyCreateModel[] model)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var id = identity.FindFirst("Id").Value;
                foreach (var pdk in model)
                {
                    pdk.IdNhanVien = Guid.Parse(id);
                }
            }
            var resule = await _IDangKyMuaHangServices.createPhieuDangKyMuaHang(model);
            return Helper.TransformData(resule);
        }

        /// <summary>
        /// Sửa phiếu đăng ký mua hàng
        /// </summary>
        /// <param name="Id">Id bản ghi</param>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPut, Route("Update")]
        public async Task<IActionResult> updatePhieuDangKyMuaHang(Guid Id, [FromQuery] PhieuDangKyUpdateModel model)
        {
            var resule = await _IDangKyMuaHangServices.updatePhieuDangKyMuaHangg(Id, model);
            return Helper.TransformData(resule);
        }

        /// <summary>
        /// Xóa phiếu đăng ký
        /// </summary>
        /// <param name="Id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpDelete, Route("{Id}/Delete")]
        public async Task<IActionResult> deletePhieuDangKyMuaHang(Guid Id)
        {
            var resule = await _IDangKyMuaHangServices.deletePhieuDangKyMuaHang(Id);
            return Helper.TransformData(resule);
        }
    }
}
