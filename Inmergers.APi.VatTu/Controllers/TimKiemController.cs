﻿using AutoMapper;
using Inmergers.Business.Service.TimKiem;
using Inmergers.Common;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Inmergers.APi.VatTu.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/[controller]")]
    public class TimKiemController : ControllerBase
    {
        private readonly vattudbContext _vatuVattudbContext;
        private readonly IMapper _mapper;
        private readonly ITimKiemServices _IMatHangServices;

        public TimKiemController(vattudbContext vattudbContext, IMapper mapper, ITimKiemServices IMatHangServices)
        {
            _vatuVattudbContext = vattudbContext;
            _mapper = mapper;
            _IMatHangServices = IMatHangServices;
        }

        /// <summary>
        /// lấy danh sách mặt hàng
        /// </summary>
        /// <param name="matHang"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("Get")]
        public async Task<IActionResult> GetTimKiem([FromQuery] TimKiemQueryModel timkiem)
        {
            var matHangRepo = await _IMatHangServices.TimKiemFullText(timkiem);
            return Helper.TransformData(matHangRepo);
        }
    }
}
