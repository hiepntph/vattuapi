﻿using Inmergers.Business.Service.ThongKe;
using Inmergers.Common;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Inmergers.APi.VatTu.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    public class ThongKeController : ControllerBase
    {
        private readonly IThongKeServices _IThongkeServices;
        private readonly vattudbContext _context;

        public ThongKeController(vattudbContext Context, IThongKeServices IthongkeServices)
        {
            _IThongkeServices = IthongkeServices;
            _context = Context;
        }

        /// <summary>
        /// Thống kê số lượng nhân viên mới theo tháng
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("ThongKeNhanVienTheoThang")]
        public async Task<IActionResult> ThongKeNhanVienTheoThang()
        {
            var result = await _IThongkeServices.ThongKeSoLuongNhanVienTheoThang();
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Thống kê số lượng nhân viên theo phòng ban
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("ThongKeSoLuongNhanVienTrongBoPhan")]
        public async Task<IActionResult> ThongKeSoLuongNhanVienTrongBoPhan()
        {
            var result = await _IThongkeServices.ThongKeSoLuongNhanVienTrongBoPhan();
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Thống kê số lượng đăng kí theo tháng
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("ThongKeSoLuongSanPhamDangkyTheoThang")]
        public async Task<IActionResult> ThongKeSoLuongSanPhamDangkyTheoThang()
        {
            var result = await _IThongkeServices.ThongKeSoLuongSanPhamDangkyTheoThang();
            return Helper.TransformData(result);
        }
    }
}
