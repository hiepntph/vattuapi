﻿using AutoMapper;
using Inmergers.Business.Hubs.Models;
using Inmergers.Business.Service.NhanVien;
using Inmergers.Common;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace Inmergers.APi.VatTu.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/[controller]")]
    public class DangNhapController : ControllerBase
    {
        private readonly vattudbContext _vatuVattudbContext;
        private readonly IMapper _mapper;
        private readonly AppSetting _appSettings;
        private readonly INhanVienServices _inhanVienServices;
        public DangNhapController(vattudbContext vattudbContext, IMapper mapper, IOptionsMonitor<AppSetting> optionsMonitor, INhanVienServices inhaNhanVienServices)
        {
            _vatuVattudbContext = vattudbContext;
            _mapper = mapper;
            _appSettings = optionsMonitor.CurrentValue;
            _inhanVienServices = inhaNhanVienServices;
        }

        /// <summary>
        /// Đăng nhập
        /// </summary>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns></returns>
        [HttpPost, Route("Login")]
        public async Task<IActionResult> Login([FromQuery] dangNhap model)
        {

            var login = await _inhanVienServices.Login(model);

            return Helper.TransformData(login);
        }
        #region TokenCu
        //token là 1 chuỗi đại diện cho đối tượng hoặc thông tin bảo mật
        //token bảo mật và token xác thực
        // public IActionResult ValidatebNhanVien([FromQuery] DangNhapModels model)
        // {
        //     var user = _vatuVattudbContext.NhanViens.FirstOrDefault(a => a.TenTaiKhoan == model.tenDangNhap && model.MatKhau == a.MatKhau);
        //
        //     if (user == null)
        //     {
        //         return Ok(new ApiResponse
        //         {
        //             Success = false,
        //             Message = "Invalid username/password"
        //         });
        //     }
        //     return Ok(new ApiResponse
        //     {
        //         Success = true,
        //         Message = "Authenticate success",
        //         Data = GenerateToken(user)
        //     });
        // }
        // // tạo 1 hàm jwt token trả về dưới dạng token model
        // private TokenModel GenerateToken(NhanVien NhanVien)
        // {
        //     //cung cấp các phương thức để xử lý tạo và ghi jwt,
        //     var jwtTokenHandler = new JwtSecurityTokenHandler();
        //     //tạo mã khóa bí mật lấy từ secretkey
        //     var secretkeyBytes = Encoding.UTF8.GetBytes(_appSettings.Secretkey);
        //         // chứa thông tin về người dùng mà token được tạo cho.
        //     var tokenDescription = new SecurityTokenDescriptor
        //     {
        //         Subject =
        //             //tạo mới biểu diễn thông tin người dùng hoặc 1 nhóm người dùng 
        //             new ClaimsIdentity(new[]
        //         {
        //             new Claim(ClaimTypes.Name,NhanVien.HoTen),
        //             new Claim(ClaimTypes.Email,NhanVien.Email),
        //             new Claim("PassWord",NhanVien.MatKhau),
        //             new Claim("UseName", NhanVien.TenTaiKhoan),
        //             new Claim("Id", NhanVien.Id.ToString()),
        //             new Claim("TokenId", Guid.NewGuid().ToString())
        //         }),
        //          Expires = DateTime.UtcNow.AddMinutes(900),
        //          // tạo ra 1 đối tượng để ký token 
        //          // tạo ra 1 đối tượng signing bằng cách sử dụng mã hóa và thuật toán đã chỉ định 
        //         SigningCredentials = new SigningCredentials(
        //             // tạo ra 1 đối tượng với khóa mã được truyền vào là secretkeyBytes
        //             new SymmetricSecurityKey(secretkeyBytes),
        //             //chỉ định thuật toán ký số là HMAC SHA512.
        //             SecurityAlgorithms.HmacSha512Signature)
        //
        //     };
        //
        //     var token = jwtTokenHandler.CreateToken(tokenDescription);
        //     //chuyển đổi một đối tượng  sang dạng chuỗi
        //     var accessToken = jwtTokenHandler.WriteToken(token);
        //
        //     return new TokenModel
        //     {
        //         AccessToken = accessToken,
        //         RefreshToken = GenerateRefreshToken()
        //     };
        // }
        //
        // // tạo 1 đoạn khóa bảo mật với kí tự ngẫu nhiên 
        // private string GenerateRefreshToken()
        // {
        //     var ramdom = new byte[50];
        //     using (var rng = RandomNumberGenerator.Create())
        //     {
        //         rng.GetBytes(ramdom);
        //
        //         return Convert.ToBase64String(ramdom);
        //     }
        // }



        #endregion
    }
}
