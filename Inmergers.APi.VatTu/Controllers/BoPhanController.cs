﻿using AutoMapper;
using Inmergers.Business.Service.BoPhan;
using Inmergers.Common;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Inmergers.APi.VatTu.Controllers
{
    //[Authorize(Roles = "hiep")]
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/[controller]")]
    public class BoPhanController : ControllerBase
    {

        private readonly vattudbContext _vatuVattudbContext;
        private readonly IMapper _mapper;
        private readonly IBoPhanServices _iBoPhanServices;

        public BoPhanController(vattudbContext vattudbContext, IMapper mapper, IBoPhanServices iBoPhanServices)
        {
            _vatuVattudbContext = vattudbContext;
            _mapper = mapper;
            _iBoPhanServices = iBoPhanServices;
        }

        /// <summary>
        /// lấy danh sách bộ phận
        /// </summary>
        /// <param name="boPhan"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("Get")]
        public async Task<IActionResult> getBoPhan([FromQuery] BoPhanQueryModel model)
        {
            var boPhanRepo = await _iBoPhanServices.getBoPhan(model);
            return Helper.TransformData(boPhanRepo);
        }

        /// <summary>
        /// lấy danh sách bộ phận bằng Id
        /// </summary>
        /// <param name="Id">Id bản ghi</param>
        /// <returns>kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("{Id}/GetById")]
        public async Task<IActionResult> getIdBoPhan(Guid Id)
        {
            var result = await _iBoPhanServices.getIdBoPhan(Id);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Thêm mới bộ phận
        /// </summary>
        /// <param name="boPhan">Dữ liệu bản ghi</param>
        /// <returns>kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPost, Route("Create")]
        public async Task<IActionResult> createBoPhan([FromBody] BoPhanCreateAndUpdateModel model)
        {
            #region thêm bộ phận khi đăng nhập
            // var identity = HttpContext.User.Identity as ClaimsIdentity;
            // if (identity != null)
            // {
            //     var username = identity.FindFirst("TenTk").Value;
            //     var idbophan = identity.FindFirst("IdBoPhan").Value;
            //     boPhan.tenNguoiDungDau = username;
            //     boPhan.tenBoPhan = idbophan;
            // }
            #endregion
            var result = await _iBoPhanServices.createBoPhan(model);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Sửa thông tin bộ phận
        /// </summary>
        /// <param name="boPhan">Dữ liệu bản ghi</param>
        /// <param name="Id">Id bản ghi</param>
        /// <returns>kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPut, Route("Update/{Id}")]
        public async Task<IActionResult> updateBoPhan(Guid Id, [FromQuery] BoPhanCreateAndUpdateModel model)
        {
            var resule = await _iBoPhanServices.updateBoPhan(Id, model);
            return Helper.TransformData(resule);
        }

        /// <summary>
        /// Xóa bộ phận
        /// </summary>
        /// <param name="Id">Id bản ghi</param>
        /// <returns>kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpDelete, Route("Delete/{Id}")]
        public async Task<IActionResult> deleteBoPhan(Guid Id)
        {
            var resule = await _iBoPhanServices.deleteBoPhan(Id);
            return Helper.TransformData(resule);
        }
    }
}
