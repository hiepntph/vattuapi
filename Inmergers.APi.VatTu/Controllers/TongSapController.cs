﻿using Inmergers.Business.Service.TongSap;
using Inmergers.Common;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Inmergers.APi.VatTu.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    public class TongSapController : ControllerBase
    {
        private readonly ITongSapServices _iTongSapServices;
        private readonly vattudbContext _context;

        public TongSapController(vattudbContext context, ITongSapServices ITongSap)
        {
            _iTongSapServices = ITongSap;
            _context = context;
        }

        /// <summary>
        /// Phòng ban đăng kí nhiều sản phẩm nhất
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("PBDangKyNhieuSanPhamNhat")]
        public async Task<IActionResult> PBDangKyNhieuSanPhamNhat()
        {
            var result = await _iTongSapServices.PBDangKyNhieuSanPhamNhat();
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Sản phẩm được đăng kí số lượng nhiều nhất
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("SpDangKyNhieuNhat")]
        public async Task<IActionResult> SpDangKyNhieuNhat()
        {
            var result = await _iTongSapServices.SpDangKyNhieuNhat();
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Tháng năm có số lượng nhân viên mới nhiều nhất
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("NhieuNhanVienNhat")]
        public async Task<IActionResult> ThangNamNhieuNhatVienMoi()
        {
            var result = await _iTongSapServices.ThangNamCoNhieuNhieuNhanVienNhat();
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Tháng năm có số lượng sản phẩm được đăng kí nhiều nhất
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("ThangNamNhieuSanPhamNhat")]
        public async Task<IActionResult> ThangNamNhieuSanPhamNhat()
        {
            var result = await _iTongSapServices.ThangNamCoNhieuNhieuSanPhamDangKyNhat();
            return Helper.TransformData(result);
        }
    }
}
