﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inmergers.Data.Data.Entity
{
    [Table("ChiTietPhieuMuaHang")]
    public class ChiTietPhieuMuaHang
    {
        [Key]
        public Guid Id { get; set; }
        public int SoLuong { get; set; }

        [ForeignKey("IdPhieuDangKyMuaHang")]
        public virtual PhieuDangKyMuaHang PhieuDangKyMuaHang { get; set; }
        public Guid IdPhieuDangKyMuaHang { get; set; }


        public Guid IdMatHang { get; set; }
        [ForeignKey("IdMatHang")]
        public virtual MatHang MatHang { get; set; }


    }
}
