﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inmergers.Data.Data.Entity
{
    [Table("NhanVien")]
    public class NhanVien
    {
        public Guid Id { get; set; }
        public string Hoten { get; set; }
        public string TenTk { get; set; }
        public string matkhau { get; set; }
        public string gmail { get; set; }
        public DateTime ngaytao { get; set; }
        public Guid IdBoPhan { get; set; }
        [ForeignKey("IdBoPhan")]
        public boPhan boPhan { get; set; }
    }
}
