﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inmergers.Data.Data.Entity
{
    [Table("PhieuDangKyMuaHang")]
    public class PhieuDangKyMuaHang
    {
        [Key]
        public Guid Id { get; set; }
        [ForeignKey("IdNhanVien")]
        public virtual NhanVien NhanVien { get; set; }
        public Guid IdNhanVien { get; set; }
        public DateTime NgayTao { get; set; }
        public ICollection<ChiTietPhieuMuaHang> ChiTietPhieuMuaHang { get; set; }
    }
}
