﻿using NpgsqlTypes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inmergers.Data.Data.Entity
{
    [Table("TimKiem")]
    public class TimKiem
    {
        [Key]
        public Guid Id { get; set; }
        public string TieuDe { get; set; }
        public string MoTa { get; set; }
        public NpgsqlTsVector TimKiemFulllText { get; set; }
    }
}
