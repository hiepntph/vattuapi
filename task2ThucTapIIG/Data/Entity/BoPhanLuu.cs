﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inmergers.Data.Data.Entity
{
    [Table("BoPhanLuu")]
    public class boPhanLuu
    {
        [Key]
        public Guid Id { get; set; }
        public string TenBoPhanLuu { get; set; }
        public int SoLuong { get; set; }
    }
}
