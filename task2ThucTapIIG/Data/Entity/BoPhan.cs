﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inmergers.Data.Data.Entity
{
    [Table("BoPhan")]
    public class boPhan
    {
        [Key]
        public Guid Id { get; set; }
        public int Stt { get; set; }
        public string tenNguoiDungDau { get; set; }
        public string tenBoPhan { get; set; }
        public DateTime ngayTao { get; set; }
        public ICollection<NhanVien> nhanvien { get; set; }
    }
}
