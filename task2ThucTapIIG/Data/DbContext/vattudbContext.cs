﻿using Inmergers.Data.Data.Entity;
using Microsoft.EntityFrameworkCore;
using System;

namespace Inmergers.Data.Data.DbContext
{
    public class vattudbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public vattudbContext()
        {

        }
        public vattudbContext(DbContextOptions<vattudbContext> options) : base(options)
        {

        }
        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
        //     IConfigurationRoot configuration = new ConfigurationBuilder()
        //         .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
        //         .AddJsonFile("appsettings.json")
        //         .Build();
        //     optionsBuilder.UseSqlServer(configuration.GetConnectionString("ketnoi"));
        //     // optionsBuilder.UseSqlServer(
        //     //      "Data Source=LAPTOP-E9710JQ8\\SQLEXPRESS;Initial Catalog=Testlab2;Persist Security Info=True;User ID=hiep1;Password=hiep1234");
        // }

        public DbSet<boPhan> boPhans { get; set; }
        public DbSet<boPhanLuu> boPhanLuus { get; set; }
        public DbSet<TimKiem> TimKiems { get; set; }
        public DbSet<MatHang> matHangs { get; set; }
        public DbSet<PhieuDangKyMuaHang> phieuDangKyMuaHangs { get; set; }
        public DbSet<NhanVien> NhanViens { get; set; }
        public DbSet<ChiTietPhieuMuaHang> ChiTietPhieuMuaHangs { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<boPhan>().HasData(
                new boPhan()
                {
                    Id = Guid.Parse("c020f595-16f8-40a8-bd45-1501b517735f"),
                    tenNguoiDungDau = "Hiep",
                    ngayTao = DateTime.Now,
                    Stt = 1,
                    tenBoPhan = "Kynh doanh",
                },

                    new boPhan()
                    {
                        Id = Guid.Parse("638ee9c7-d468-4be6-999c-0b313144a58c"),
                        tenNguoiDungDau = "Hai",
                        ngayTao = DateTime.Now,
                        Stt = 2,
                        tenBoPhan = "IT",
                    },

                        new boPhan()
                        {
                            Id = Guid.Parse("91feaa95-9a22-406d-ac57-c33ac53670ca"),
                            tenNguoiDungDau = "Bach",
                            ngayTao = DateTime.Now,
                            Stt = 3,
                            tenBoPhan = "Chien Luoc",
                        }
                    );
            modelBuilder.Entity<MatHang>().HasData(
                new MatHang()
                {
                    Id = Guid.Parse("b86ad435-a670-4a12-aaaf-9e4b9d033463"),
                    Stt = 1,
                    tenMatHang = "Kyem",
                },
                new MatHang()
                {
                    Id = Guid.Parse("fbd25f0c-6812-4eb8-9597-952be09ed8a8"),
                    Stt = 2,
                    tenMatHang = "Dao",
                },
                new MatHang()
                {
                    Id = Guid.Parse("e6740993-4baa-4f48-b2d8-912fc1bb16f1"),
                    Stt = 3,
                    tenMatHang = "Pho",
                });
            modelBuilder.Entity<NhanVien>().HasData(
                new NhanVien()
                {
                    Id = Guid.Parse("62441159-c1cf-47e9-b06c-c8858d7012aa"),
                    Hoten = "hiep",
                    TenTk = "hiep",
                    matkhau = "hiep",
                    gmail = "hiep312@gmail.com",
                    ngaytao = DateTime.Now,
                    IdBoPhan = Guid.Parse("638ee9c7-d468-4be6-999c-0b313144a58c")
                },
                new NhanVien()
                {
                    Id = Guid.Parse("5281c629-97f7-4ea8-a08b-2b60882727a3"),
                    Hoten = "hai",
                    TenTk = "hai",
                    matkhau = "hai",
                    gmail = "hai312@gmail.com",
                    ngaytao = DateTime.Now,
                    IdBoPhan = Guid.Parse("638ee9c7-d468-4be6-999c-0b313144a58c")
                },
                new NhanVien()
                {
                    Id = Guid.Parse("31445c91-cfcc-488f-944b-7b13ae051b3e"),
                    Hoten = "bach",
                    TenTk = "bach",
                    matkhau = "bach",
                    gmail = "bach312@gmail.com",
                    ngaytao = DateTime.Now,
                    IdBoPhan = Guid.Parse("c020f595-16f8-40a8-bd45-1501b517735f")
                });
            modelBuilder.Entity<PhieuDangKyMuaHang>().HasData(
                new PhieuDangKyMuaHang()
                {
                    Id = Guid.Parse("c020f595-16f8-40a8-bd45-1501b517735f"),
                    IdNhanVien = Guid.Parse("62441159-c1cf-47e9-b06c-c8858d7012aa"),
                    NgayTao = DateTime.Now,

                },
                new PhieuDangKyMuaHang()
                {
                    Id = Guid.Parse("638ee9c7-d468-4be6-999c-0b313144a58c"),
                    IdNhanVien = Guid.Parse("5281c629-97f7-4ea8-a08b-2b60882727a3"),
                    NgayTao = DateTime.Now,
                },
                new PhieuDangKyMuaHang()
                {
                    Id = Guid.Parse("91feaa95-9a22-406d-ac57-c33ac53670ca"),
                    IdNhanVien = Guid.Parse("31445c91-cfcc-488f-944b-7b13ae051b3e"),
                    NgayTao = DateTime.Now,
                });
            modelBuilder.Entity<ChiTietPhieuMuaHang>().HasData(
                new ChiTietPhieuMuaHang()
                {
                    Id = Guid.Parse("c020f595-16f8-40a8-bd45-1501b517735f"),
                    IdPhieuDangKyMuaHang = Guid.Parse("91feaa95-9a22-406d-ac57-c33ac53670ca"),
                    SoLuong = 30,
                    IdMatHang = Guid.Parse("b86ad435-a670-4a12-aaaf-9e4b9d033463")
                },
                new ChiTietPhieuMuaHang()
                {
                    Id = Guid.Parse("638ee9c7-d468-4be6-999c-0b313144a58c"),
                    IdPhieuDangKyMuaHang = Guid.Parse("638ee9c7-d468-4be6-999c-0b313144a58c"),
                    SoLuong = 20,
                    IdMatHang = Guid.Parse("e6740993-4baa-4f48-b2d8-912fc1bb16f1")

                },
                new ChiTietPhieuMuaHang()
                {
                    Id = Guid.Parse("91feaa95-9a22-406d-ac57-c33ac53670ca"),
                    IdPhieuDangKyMuaHang = Guid.Parse("638ee9c7-d468-4be6-999c-0b313144a58c"),
                    SoLuong = 10,
                    IdMatHang = Guid.Parse("e6740993-4baa-4f48-b2d8-912fc1bb16f1")

                });
            modelBuilder.Entity<TimKiem>()
                .Ignore(t => t.TimKiemFulllText);

            base.OnModelCreating(modelBuilder);

        }
    }
}
