﻿using System;
using System.Collections.Generic;
using System.Text;
using Inmergers.Data.Infrastructure.repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Inmergers.Data.Infrastructure
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private readonly DbContext _dataContext;
        private readonly IConfiguration _configuration;

        public DatabaseFactory()
        {
            // _dataContext = new ImDbContext();
        }

        public DbContext GetDbContext()
        {
            return _dataContext;
        }
    }
}
