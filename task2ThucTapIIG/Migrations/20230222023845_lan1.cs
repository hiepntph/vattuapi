﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Inmergers.Data.Migrations
{
    public partial class lan1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BoPhan",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Stt = table.Column<int>(nullable: false),
                    tenNguoiDungDau = table.Column<string>(nullable: true),
                    tenBoPhan = table.Column<string>(nullable: true),
                    ngayTao = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoPhan", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BoPhanLuu",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TenBoPhanLuu = table.Column<string>(nullable: true),
                    SoLuong = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoPhanLuu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MatHang",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    tenMatHang = table.Column<string>(nullable: true),
                    Stt = table.Column<int>(nullable: false),
                    soLuong = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatHang", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TimKiem",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TieuDe = table.Column<string>(nullable: true),
                    MoTa = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimKiem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NhanVien",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Hoten = table.Column<string>(nullable: true),
                    TenTk = table.Column<string>(nullable: true),
                    matkhau = table.Column<string>(nullable: true),
                    gmail = table.Column<string>(nullable: true),
                    ngaytao = table.Column<DateTime>(nullable: false),
                    IdBoPhan = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NhanVien", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NhanVien_BoPhan_IdBoPhan",
                        column: x => x.IdBoPhan,
                        principalTable: "BoPhan",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PhieuDangKyMuaHang",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IdNhanVien = table.Column<Guid>(nullable: false),
                    NgayTao = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhieuDangKyMuaHang", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhieuDangKyMuaHang_NhanVien_IdNhanVien",
                        column: x => x.IdNhanVien,
                        principalTable: "NhanVien",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChiTietPhieuMuaHang",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SoLuong = table.Column<int>(nullable: false),
                    IdPhieuDangKyMuaHang = table.Column<Guid>(nullable: false),
                    IdMatHang = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChiTietPhieuMuaHang", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChiTietPhieuMuaHang_MatHang_IdMatHang",
                        column: x => x.IdMatHang,
                        principalTable: "MatHang",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChiTietPhieuMuaHang_PhieuDangKyMuaHang_IdPhieuDangKyMuaHang",
                        column: x => x.IdPhieuDangKyMuaHang,
                        principalTable: "PhieuDangKyMuaHang",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "BoPhan",
                columns: new[] { "Id", "Stt", "ngayTao", "tenBoPhan", "tenNguoiDungDau" },
                values: new object[,]
                {
                    { new Guid("c020f595-16f8-40a8-bd45-1501b517735f"), 1, new DateTime(2023, 2, 22, 9, 38, 44, 873, DateTimeKind.Local).AddTicks(6952), "Kynh doanh", "Hiep" },
                    { new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), 2, new DateTime(2023, 2, 22, 9, 38, 44, 874, DateTimeKind.Local).AddTicks(5287), "IT", "Hai" },
                    { new Guid("91feaa95-9a22-406d-ac57-c33ac53670ca"), 3, new DateTime(2023, 2, 22, 9, 38, 44, 874, DateTimeKind.Local).AddTicks(5322), "Chien Luoc", "Bach" }
                });

            migrationBuilder.InsertData(
                table: "MatHang",
                columns: new[] { "Id", "Stt", "soLuong", "tenMatHang" },
                values: new object[,]
                {
                    { new Guid("b86ad435-a670-4a12-aaaf-9e4b9d033463"), 1, 0, "Kyem" },
                    { new Guid("fbd25f0c-6812-4eb8-9597-952be09ed8a8"), 2, 0, "Dao" },
                    { new Guid("e6740993-4baa-4f48-b2d8-912fc1bb16f1"), 3, 0, "Pho" }
                });

            migrationBuilder.InsertData(
                table: "NhanVien",
                columns: new[] { "Id", "Hoten", "IdBoPhan", "TenTk", "gmail", "matkhau", "ngaytao" },
                values: new object[] { new Guid("31445c91-cfcc-488f-944b-7b13ae051b3e"), "bach", new Guid("c020f595-16f8-40a8-bd45-1501b517735f"), "bach", "bach312@gmail.com", "bach", new DateTime(2023, 2, 22, 9, 38, 44, 875, DateTimeKind.Local).AddTicks(7872) });

            migrationBuilder.InsertData(
                table: "NhanVien",
                columns: new[] { "Id", "Hoten", "IdBoPhan", "TenTk", "gmail", "matkhau", "ngaytao" },
                values: new object[] { new Guid("62441159-c1cf-47e9-b06c-c8858d7012aa"), "hiep", new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), "hiep", "hiep312@gmail.com", "hiep", new DateTime(2023, 2, 22, 9, 38, 44, 875, DateTimeKind.Local).AddTicks(7294) });

            migrationBuilder.InsertData(
                table: "NhanVien",
                columns: new[] { "Id", "Hoten", "IdBoPhan", "TenTk", "gmail", "matkhau", "ngaytao" },
                values: new object[] { new Guid("5281c629-97f7-4ea8-a08b-2b60882727a3"), "hai", new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), "hai", "hai312@gmail.com", "hai", new DateTime(2023, 2, 22, 9, 38, 44, 875, DateTimeKind.Local).AddTicks(7851) });

            migrationBuilder.InsertData(
                table: "PhieuDangKyMuaHang",
                columns: new[] { "Id", "IdNhanVien", "NgayTao" },
                values: new object[] { new Guid("91feaa95-9a22-406d-ac57-c33ac53670ca"), new Guid("31445c91-cfcc-488f-944b-7b13ae051b3e"), new DateTime(2023, 2, 22, 9, 38, 44, 875, DateTimeKind.Local).AddTicks(9289) });

            migrationBuilder.InsertData(
                table: "PhieuDangKyMuaHang",
                columns: new[] { "Id", "IdNhanVien", "NgayTao" },
                values: new object[] { new Guid("c020f595-16f8-40a8-bd45-1501b517735f"), new Guid("62441159-c1cf-47e9-b06c-c8858d7012aa"), new DateTime(2023, 2, 22, 9, 38, 44, 875, DateTimeKind.Local).AddTicks(8951) });

            migrationBuilder.InsertData(
                table: "PhieuDangKyMuaHang",
                columns: new[] { "Id", "IdNhanVien", "NgayTao" },
                values: new object[] { new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), new Guid("5281c629-97f7-4ea8-a08b-2b60882727a3"), new DateTime(2023, 2, 22, 9, 38, 44, 875, DateTimeKind.Local).AddTicks(9265) });

            migrationBuilder.InsertData(
                table: "ChiTietPhieuMuaHang",
                columns: new[] { "Id", "IdMatHang", "IdPhieuDangKyMuaHang", "SoLuong" },
                values: new object[] { new Guid("c020f595-16f8-40a8-bd45-1501b517735f"), new Guid("b86ad435-a670-4a12-aaaf-9e4b9d033463"), new Guid("91feaa95-9a22-406d-ac57-c33ac53670ca"), 30 });

            migrationBuilder.InsertData(
                table: "ChiTietPhieuMuaHang",
                columns: new[] { "Id", "IdMatHang", "IdPhieuDangKyMuaHang", "SoLuong" },
                values: new object[] { new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), new Guid("e6740993-4baa-4f48-b2d8-912fc1bb16f1"), new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), 20 });

            migrationBuilder.InsertData(
                table: "ChiTietPhieuMuaHang",
                columns: new[] { "Id", "IdMatHang", "IdPhieuDangKyMuaHang", "SoLuong" },
                values: new object[] { new Guid("91feaa95-9a22-406d-ac57-c33ac53670ca"), new Guid("e6740993-4baa-4f48-b2d8-912fc1bb16f1"), new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), 10 });

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietPhieuMuaHang_IdMatHang",
                table: "ChiTietPhieuMuaHang",
                column: "IdMatHang");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietPhieuMuaHang_IdPhieuDangKyMuaHang",
                table: "ChiTietPhieuMuaHang",
                column: "IdPhieuDangKyMuaHang");

            migrationBuilder.CreateIndex(
                name: "IX_NhanVien_IdBoPhan",
                table: "NhanVien",
                column: "IdBoPhan");

            migrationBuilder.CreateIndex(
                name: "IX_PhieuDangKyMuaHang_IdNhanVien",
                table: "PhieuDangKyMuaHang",
                column: "IdNhanVien");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BoPhanLuu");

            migrationBuilder.DropTable(
                name: "ChiTietPhieuMuaHang");

            migrationBuilder.DropTable(
                name: "TimKiem");

            migrationBuilder.DropTable(
                name: "MatHang");

            migrationBuilder.DropTable(
                name: "PhieuDangKyMuaHang");

            migrationBuilder.DropTable(
                name: "NhanVien");

            migrationBuilder.DropTable(
                name: "BoPhan");
        }
    }
}
