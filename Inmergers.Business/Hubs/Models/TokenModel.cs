﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inmergers.Business.Hubs.Models
{
    public class TokenModel
    {
        public string AccessToken { get; set; } // là 1 mã định danh ngắn để xác nhận người dùng trong 1 thời gian nhất định , chứa thông tin người dùng và được mã hóa nó bằng thuật toán mã hóa 
        public string RefreshToken { get; set; } //là 1 mã 
    }
}
