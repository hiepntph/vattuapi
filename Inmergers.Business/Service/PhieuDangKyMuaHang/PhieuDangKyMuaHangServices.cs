﻿using AutoMapper;
using Inmergers.Common;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Code = System.Net.HttpStatusCode;

namespace Inmergers.Business.Service.PhieuDangKyMuaHang
{
    public class PhieuDangKyMuaHangServices : IPhieuDangKyMuaHangServices
    {
        private readonly vattudbContext _Context;
        private readonly IMapper _Mapper;

        public PhieuDangKyMuaHangServices(vattudbContext context, IMapper mapper)
        {
            _Context = context ?? throw new ArgumentNullException(nameof(context));
            _Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper)); ;
        }
        public async Task<Response> createPhieuDangKyMuaHang(PhieuDangKyCreateModel model)
        {
            try
            {
                //Validate
                var SoLuong = model.MatHangCreate.Where(c => c.soluong <= 0);
                if (SoLuong.Count() > 0)
                    return new Response(Code.BadRequest, "Số lượng mặt hàng phải lớn hơn 0");

                var entity = _Mapper.Map<Data.Data.Entity.PhieuDangKyMuaHang>(model);
                _Context.Add(entity);
                await _Context.SaveChangesAsync();
                var result = _Mapper.Map<PhieuDangKyMuaHangModels>(entity);
                return new ResponseObject<PhieuDangKyMuaHangModels>(result);
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);

            }
        }

        public async Task<Response> createPhieuDangKyMuaHang(PhieuDangKyCreateModel[] model)
        {
            try
            {
                var entity = new List<Data.Data.Entity.PhieuDangKyMuaHang>();
                foreach (var item in model)
                {
                    entity.Add(new Data.Data.Entity.PhieuDangKyMuaHang() { IdNhanVien = item.IdNhanVien, NgayTao = DateTime.Now });
                }

                await _Context.AddRangeAsync(entity);
                var status = await _Context.SaveChangesAsync();
                if (status > 0)
                {
                    var data = _Mapper.Map<Data.Data.Entity.PhieuDangKyMuaHang, PhieuDangKyMuaHangModels>(new Data.Data.Entity.PhieuDangKyMuaHang() { IdNhanVien = model.First().IdNhanVien });
                    return new ResponseObject<PhieuDangKyMuaHangModels>(data, "Thêm thành công");
                }
                return new ResponseError(Code.BadRequest, "Thêm thất bại");
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> updatePhieuDangKyMuaHangg(Guid Id, PhieuDangKyUpdateModel model)
        {
            try
            {
                var entityModel = await _Context.phieuDangKyMuaHangs.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entityModel == null)
                {
                    return new ResponseError(Code.BadRequest, "Không tìm thấy Id");
                }

                entityModel.IdNhanVien = model.IdNhanVien;
                var status = await _Context.SaveChangesAsync();
                if (status > 0)
                {

                    var data = _Mapper.Map<Data.Data.Entity.PhieuDangKyMuaHang, PhieuDangKyMuaHangModels>(entityModel);
                    return new ResponseObject<PhieuDangKyMuaHangModels>(data, "Sửa thành công");
                }

                return new ResponseError(Code.BadRequest, "Sửa thất bại");
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> getPhieuDangKyMuaHangg(PhieuDangKyQueryModel filter)
        {
            try
            {
                var predicate = BuildQueryPhieuDangKy(filter);
                var result = _Context.phieuDangKyMuaHangs.Include(c => c.NhanVien).GetPage(filter);
                var PhieuDangKyDTo =
                    JsonConvert.DeserializeObject<Pagination<PhieuDangKyMuaHangModels>>(JsonConvert.SerializeObject(result));
                return new ResponsePagination<PhieuDangKyMuaHangModels>(PhieuDangKyDTo);
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> getIPhieuDangKyMuaHangg(Guid Id)
        {
            try
            {
                var entity = await _Context.phieuDangKyMuaHangs
                    .Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseError(Code.BadRequest, "Không tìm thấy id ");
                }
                var data = _Mapper.Map<Data.Data.Entity.PhieuDangKyMuaHang, PhieuDangKyMuaHangModels>(entity);
                return new ResponseObject<PhieuDangKyMuaHangModels>(data);

            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);

            }
        }

        public async Task<Response> deletePhieuDangKyMuaHang(Guid Id)
        {
            try
            {
                var entity = await _Context.phieuDangKyMuaHangs.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseError(Code.BadRequest, "Không tìm thấy id để xóa");
                }

                _Context.Remove(entity);
                _Context.SaveChangesAsync();
                var ten = entity.IdNhanVien.ToString();
                return new ResponseDelete(Code.OK, "Xóa thành công", Id, ten);
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);

            }
        }
        private Expression<Func<Data.Data.Entity.PhieuDangKyMuaHang, bool>> BuildQueryPhieuDangKy(PhieuDangKyQueryModel query)
        {
            var predicate = PredicateBuilder.New<Data.Data.Entity.PhieuDangKyMuaHang>(true);

            if (query.phieuDangKyPhanId.HasValue && query.phieuDangKyPhanId.Value != Guid.Empty)
            {
                predicate = predicate.And(c => c.Id == query.phieuDangKyPhanId);
            }

            return predicate;
        }

    }
}
