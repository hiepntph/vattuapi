﻿using Inmergers.Common.Utils;
using System;
using System.Collections.Generic;

namespace Inmergers.Business.Service.PhieuDangKyMuaHang
{
    public class PhieuDangKyMuaHangModels
    {
        public Guid Id { get; set; }
        public Guid IdNhanVien { get; set; }
        public List<ChiTietPhieuDangKyModel> ChiTietPhieuDangKy { get; set; }
    }
    public class PhieuDangKyCreateModel
    {
        public Guid IdNhanVien { get; set; }
        public List<ChiTietPhieuDangKyCreate> MatHangCreate { get; set; }

    }

    public class ChiTietPhieuDangKyModel
    {
        public Guid Id { get; set; }
        public int SoLuong { get; set; }
        public Guid IdMatHang { get; set; }
    }
    public class ChiTietPhieuDangKyCreate
    {
        public Guid IdMatHang { get; set; }
        public int soluong { get; set; }

    }
    public class ChiTietPhieu
    {
        public int soluongban { get; set; }

    }

    public class PhieuDangKyUpdateModel
    {
        public Guid IdNhanVien { get; set; }
    }

    public class PhieuDangKyQueryModel : PaginationRequest
    {
        public Guid? phieuDangKyPhanId { get; set; }
    }

}
