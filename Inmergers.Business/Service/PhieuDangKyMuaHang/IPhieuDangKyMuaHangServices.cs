﻿using Inmergers.Common.Utils;
using System;
using System.Threading.Tasks;

namespace Inmergers.Business.Service.PhieuDangKyMuaHang
{
    public interface IPhieuDangKyMuaHangServices
    {
        Task<Response> createPhieuDangKyMuaHang(PhieuDangKyCreateModel model);
        Task<Response> createPhieuDangKyMuaHang(PhieuDangKyCreateModel[] model);
        Task<Response> updatePhieuDangKyMuaHangg(Guid Id, PhieuDangKyUpdateModel model);
        Task<Response> getPhieuDangKyMuaHangg(PhieuDangKyQueryModel filter);
        Task<Response> getIPhieuDangKyMuaHangg(Guid Id);
        Task<Response> deletePhieuDangKyMuaHang(Guid Id);

    }
}
