﻿using Inmergers.Common.Utils;
using System.Threading.Tasks;

namespace Inmergers.Business.Service.TimKiem
{
    public interface ITimKiemServices
    {
        Task<Response> TimKiemFullText(TimKiemQueryModel query);
    }
}
