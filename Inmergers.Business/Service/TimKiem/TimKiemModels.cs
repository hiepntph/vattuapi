﻿using Inmergers.Common.Utils;
using System;

namespace Inmergers.Business.Service.TimKiem
{
    public class TimKiemModels
    {
        public Guid Id { get; set; }
        public string TieuDe { get; set; }
        public string MoTa { get; set; }
    }
    public class TimKiemQueryModel : PaginationRequest
    {
        public Guid? TimKiemId { get; set; }
        public string PreviousFullTextSearch { get; set; }
    }

}

