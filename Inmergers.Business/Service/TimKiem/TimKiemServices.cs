﻿using AutoMapper;
using Inmergers.Common;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Inmergers.Business.Service.TimKiem
{
    public class TimKiemServices : ITimKiemServices
    {
        private readonly vattudbContext _Context;
        private readonly IMapper _Mapper;
        public TimKiemServices(vattudbContext Context, IMapper IMapper)
        {
            _Context = Context ?? throw new ArgumentNullException(nameof(Context));
            _Mapper = IMapper ?? throw new ArgumentNullException(nameof(IMapper));
        }
        public async Task<Response> TimKiemFullText(TimKiemQueryModel query)
        {
            try
            {
                var predicate = BuildQuery(query);

                var result = _Context.TimKiems
                    .OrderByDescending(p => EF.Functions.ToTsVector("english", p.TieuDe)
                        .Rank(EF.Functions.PlainToTsQuery("english", query.FullTextSearch)))
                    .Where(predicate).GetPage(query);
                var timkiemDTo =
                    JsonConvert.DeserializeObject<Pagination<TimKiemModels>>(JsonConvert.SerializeObject(result));
                return new ResponsePagination<TimKiemModels>(timkiemDTo);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lấy danh sách bản tin không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        private Expression<Func<Data.Data.Entity.TimKiem, bool>> BuildQuery(TimKiemQueryModel query)
        {
            var predicate = PredicateBuilder.New<Data.Data.Entity.TimKiem>(true);
            var fullTextQuery = UtilsGetConfig.RemoveVietnameseSign(query.FullTextSearch);
            fullTextQuery = Regex.Escape(fullTextQuery);
            fullTextQuery = Regex.Replace(fullTextQuery, @"\s+", "%");
            fullTextQuery = fullTextQuery.Trim();
            if (string.IsNullOrEmpty(fullTextQuery))
            {
                return PredicateBuilder.New<Data.Data.Entity.TimKiem>(true);
            }

            var keywords = Regex.Matches(fullTextQuery, @"\b\w+\b")
                .Cast<Match>()
                .Select(m => m.Value)
                .Distinct()
                .ToArray();

            var keywordPredicates = new List<Expression<Func<Data.Data.Entity.TimKiem, bool>>>();
            foreach (var keyword in keywords)
            {
                var tsQuery = $"{keyword}:*";
                var keywordPredicate = PredicateBuilder.New<Data.Data.Entity.TimKiem>()
                    .And(p => EF.Functions.ToTsVector("english", p.MoTa).Matches(EF.Functions.ToTsQuery("english", tsQuery)))
                    .Or(p => EF.Functions.ToTsVector("english", p.MoTa).Matches(EF.Functions.ToTsQuery("english", $"{keyword}*")));
                keywordPredicates.Add(keywordPredicate);
            }

            predicate = predicate.Or(keywordPredicates.Aggregate((acc, next) => acc.Or(next)));
            return predicate;
        }
    }
}

