﻿using Inmergers.Common.Utils;
using System;
using System.Threading.Tasks;

namespace Inmergers.Business.Service.NhanVien
{
    public interface INhanVienServices
    {
        Task<Response> Login(dangNhap model);
        Task<Response> createNhanVien(CreateNhanvienAndUpdate model);
        Task<Response> updateNhanVien(Guid Id, CreateNhanvienAndUpdate model);
        Task<Response> getNhanVien(NhanVienQueryModel filter);
        Task<Response> getIdNhanVien(Guid Id);
        Task<Response> deleteNhanVien(Guid Id);
    }
}
