﻿using Inmergers.Common.Utils;
using System;

namespace Inmergers.Business.Service.NhanVien
{
    public class NhanVienModel
    {
        public Guid Id { get; set; }
        public string Hoten { get; set; }
        public string TenTk { get; set; }
        public string matkhau { get; set; }
        public string gmail { get; set; }
        public DateTime ngaytao { get; set; }
        public Guid IdBoPhan { get; set; }
    }

    public class CreateNhanvienAndUpdate
    {
        public string Hoten { get; set; }
        public string TenTk { get; set; }
        public string matkhau { get; set; }
        public string gmail { get; set; }
        public DateTime ngaytao { get; set; }
        public Guid IdBoPhan { get; set; }
    }

    public class dangNhap
    {
        public string TenTk { get; set; }
        public string matkhau { get; set; }
    }

    public class NhanVienQueryModel : PaginationRequest
    {
        public Guid? IdNhanVien { get; set; }
    }
}
