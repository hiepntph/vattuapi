﻿using AutoMapper;
using Inmergers.Common;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using LinqKit;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Serilog;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Code = System.Net.HttpStatusCode;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace Inmergers.Business.Service.NhanVien
{
    public class NhanVienServices : INhanVienServices
    {
        private readonly UserManager<Data.Data.Entity.NhanVien> _userManager;
        private readonly vattudbContext _Context;
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuration;
        private readonly IMapper _Mapper;

        public NhanVienServices(vattudbContext context, Microsoft.Extensions.Configuration.IConfiguration config, IMapper IMapper)
        {
            _Context = context;
            _Mapper = IMapper;
            _configuration = config;
        }

        public async Task<Response> Login(dangNhap model)
        {
            var user = await _Context.NhanViens.FirstOrDefaultAsync(c => c.TenTk == model.TenTk);
            if (user == null)
            {
                return new ResponseError(Code.BadRequest, "không tìm thấy tài khoản");
            }

            //Tạo một mảng các đối tượng Claim để lưu trữ các thông tin về người dùng cần được đính kèm vào mã JWT
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                new Claim("TenTk", user.TenTk),
                new Claim("MatKhau", user.matkhau),
                new Claim("gmail", user.gmail),
                new Claim("hoten", user.Hoten),
                new Claim("IdBoPhan", user.IdBoPhan.ToString()),
                new Claim("NgayTao", user.ngaytao.ToShortDateString()),
                new Claim("Id", user.Id.ToString())
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:key"]));
            var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token =
                new JwtSecurityToken(_configuration["Jwt:Issuer"],
                    _configuration["Jwt:Audience"], claims, //một mảng các thông tin về người dùng được mã hóa thành các Claims
                    expires: DateTime.UtcNow.AddDays(1),//hời gian hết hạn của token, ở đây được đặt là một ngày kể từ thời điểm hiện tạ
                    signingCredentials: signIn);//
            return new ResponseObject<string>(new JwtSecurityTokenHandler().WriteToken(token));
        }

        public async Task<Response> createNhanVien(CreateNhanvienAndUpdate model)
        {
            try
            {
                //validate
                if (string.IsNullOrEmpty(model.Hoten) || model.Hoten.Length > 50)
                    return new ResponseError(Code.BadRequest, "Họ tên nhân viên không được để trống và phải dưới 50 ký tự");
                if (string.IsNullOrEmpty(model.matkhau) || string.IsNullOrEmpty(model.TenTk))
                    return new ResponseError(Code.BadRequest, "Mật khẩu và tên khoản không được để trống");

                var EntityModel = new Data.Data.Entity.NhanVien()
                {
                    TenTk = model.TenTk,
                    Hoten = model.Hoten,
                    matkhau = model.matkhau,
                    gmail = model.gmail,
                    ngaytao = DateTime.Now,
                    IdBoPhan = model.IdBoPhan
                };
                _Context.Add(EntityModel);
                var status = await _Context.SaveChangesAsync();

                if (status > 0)
                {
                    var data = _Mapper.Map<Data.Data.Entity.NhanVien, NhanVienModel>(EntityModel);
                    return new ResponseObject<NhanVienModel>(data, "Thêm thành công");
                }
                return new ResponseError(Code.BadRequest, "Thêm thất bại");
            }
            catch (Exception e)
            {
                Log.Error(e, "Lấy dữ liệu không thành công");
                return new ResponseError(Code.InternalServerError, "Có lỗi trong quá trình xử lý: " + e.Message);
            }
        }

        public async Task<Response> updateNhanVien(Guid Id, CreateNhanvienAndUpdate model)
        {
            try
            {
                //validate
                if (string.IsNullOrEmpty(model.Hoten) || model.Hoten.Length > 50)
                    return new ResponseError(Code.BadRequest, "Họ tên nhân viên không được để trống và phải dưới 50 ký tự");
                if (string.IsNullOrEmpty(model.matkhau) || string.IsNullOrEmpty(model.TenTk))
                    return new ResponseError(Code.BadRequest, "Mật khẩu và tên khoản không được để trống");

                var EntityModel = await _Context.NhanViens.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (EntityModel == null)
                {
                    return new ResponseError(Code.BadRequest, "không tìm thấy Id  người dùng");
                }

                EntityModel.ngaytao = DateTime.Now;
                EntityModel.Hoten = model.Hoten;
                EntityModel.TenTk = model.TenTk;
                EntityModel.gmail = model.gmail;
                EntityModel.matkhau = model.matkhau;
                var status = await _Context.SaveChangesAsync();

                if (status > 0)
                {
                    var data = _Mapper.Map<Data.Data.Entity.NhanVien, NhanVienModel>(EntityModel);
                    return new ResponseObject<NhanVienModel>(data, "Sửa thành công");
                }
                return new ResponseError(Code.BadRequest, "Sửa thất bại");
            }
            catch (Exception e)
            {
                Log.Error(e, "Lấy dữ liệu không thành công");
                return new ResponseError(Code.InternalServerError, "Có lỗi trong quá trình xử lý: " + e.Message);
            }
        }

        public async Task<Response> getNhanVien(NhanVienQueryModel filter)
        {
            try
            {
                var predicate = BuildQueryNhanVien(filter);
                var result = _Context.NhanViens.Where(predicate).GetPage(filter);

                var NhanVienDto =
                    JsonConvert.DeserializeObject<Pagination<NhanVienModel>>(JsonConvert.SerializeObject(result));
                return new ResponsePagination<NhanVienModel>(NhanVienDto);
            }
            catch (Exception e)
            {
                Log.Error(e, "Lấy dữ liệu không thành công");
                return new ResponseError(Code.InternalServerError, "Có lỗi trong quá trình xử lý: " + e.Message);
            }
        }


        public async Task<Response> getIdNhanVien(Guid Id)
        {
            try
            {
                var Entity = await _Context.NhanViens.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (Entity == null)
                {
                    return new ResponseError(Code.BadRequest, "Không tìm thấy Id bộ phận");
                }

                var data = _Mapper.Map<Data.Data.Entity.NhanVien, NhanVienModel>(Entity);
                return new ResponseObject<NhanVienModel>(data);
            }
            catch (Exception e)
            {
                Log.Error(e, "Lấy dữ liệu không thành công");
                return new ResponseError(Code.InternalServerError, "Có lỗi trong quá trình xử lý: " + e.Message);
            }
        }

        public async Task<Response> deleteNhanVien(Guid Id)
        {
            try
            {
                var Entity = await _Context.NhanViens.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (Entity == null)
                {
                    return new ResponseError(Code.BadRequest, "Không tìm thấy Id người dùng");
                }

                _Context.Remove(Entity);
                _Context.SaveChanges();
                var Ten = Entity.Hoten;
                return new ResponseDelete(Code.OK, "Xóa thành công", Id, Ten);
            }
            catch (Exception e)
            {
                Log.Error(e, "Lấy dữ liệu không thành công");
                return new ResponseError(Code.InternalServerError, "Có lỗi trong quá trình xử lý: " + e.Message);
            }
        }
        private Expression<Func<Data.Data.Entity.NhanVien, bool>> BuildQueryNhanVien(NhanVienQueryModel query)
        {
            var predicate = PredicateBuilder.New<Data.Data.Entity.NhanVien>(true);

            if (query.IdNhanVien.HasValue && query.IdNhanVien.Value != Guid.Empty)
            {
                predicate = predicate.And(c => c.Id == query.IdNhanVien);
            }

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(c => c.TenTk.Contains(query.FullTextSearch)
                                   || c.Hoten.Contains(query.FullTextSearch) || c.gmail.Contains(query.FullTextSearch));

            return predicate;
        }
    }
}
