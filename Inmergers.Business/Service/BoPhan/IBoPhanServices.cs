﻿using Inmergers.Common.Utils;
using System;
using System.Threading.Tasks;

namespace Inmergers.Business.Service.BoPhan
{
    public interface IBoPhanServices
    {
        Task<Response> createBoPhan(BoPhanCreateAndUpdateModel model);
        Task<Response> updateBoPhan(Guid Id, BoPhanCreateAndUpdateModel model);
        Task<Response> getBoPhan(BoPhanQueryModel filter);
        Task<Response> getIdBoPhan(Guid Id);
        Task<Response> deleteBoPhan(Guid Id);
    }
}
