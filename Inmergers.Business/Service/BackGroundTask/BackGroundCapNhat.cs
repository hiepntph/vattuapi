﻿using Inmergers.Business.Service.TongSap;
using Inmergers.Data.Data.DbContext;
using Inmergers.Data.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Inmergers.Business.Service.BackGroundTask
{
    public class BackGroundCapNhat : IBackGroundCapNhat
    {
        private readonly vattudbContext _Context;
        private readonly Microsoft.Extensions.Logging.ILogger _logger;

        public BackGroundCapNhat(vattudbContext Context, ILogger<BackGroundCapNhat> logger)
        {
            _Context = Context;
            _logger = logger;

        }

        private int solan;

        public async Task PhongBanDangKyNhieuSanPhamNhat(CancellationToken cancellationToken)
        {
            try
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    Interlocked.Increment(ref solan);
                    _logger.LogInformation($"So lan thuc hien cap nhat: {solan}");

                    var ListBoPhan = _Context.ChiTietPhieuMuaHangs
                            .Include(ct => ct.PhieuDangKyMuaHang.NhanVien.boPhan)
                            .ToList()
                            .GroupBy(ct => ct.PhieuDangKyMuaHang.NhanVien.boPhan.tenBoPhan)
                            .ToDictionary(
                                g => g.Key,
                                g => g.Sum(ct => ct.SoLuong)
                            );

                    var result = ListBoPhan
                        .Select(p => new BoPhanLuu
                        {
                            TenBoPhanLuu = p.Key,
                            SoLuong = p.Value,
                        })
                        .OrderByDescending(p => p.SoLuong)
                        .FirstOrDefault();
                    _Context.boPhanLuus.RemoveRange(_Context.boPhanLuus.Where(a => true));
                    if (result != null)
                    {
                        var entity = new boPhanLuu()
                        {
                            TenBoPhanLuu = result.TenBoPhanLuu,
                            SoLuong = result.SoLuong,
                        };
                        _Context.boPhanLuus.Add(entity);
                    }
                    _Context.SaveChanges();
                    await Task.Delay(TimeSpan.FromMinutes(15));
                }
            }
            catch (Exception e)
            {
                Log.Error("Cập nhật phòng ban nhiều đăng ký nhất không thành công ");
            }
        }
    }
}
