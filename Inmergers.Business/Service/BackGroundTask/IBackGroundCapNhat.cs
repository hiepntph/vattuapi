﻿using System.Threading;
using System.Threading.Tasks;

namespace Inmergers.Business.Service.BackGroundTask
{
    public interface IBackGroundCapNhat
    {
        Task PhongBanDangKyNhieuSanPhamNhat(CancellationToken cancellationToken);
    }
}
