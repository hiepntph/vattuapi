﻿using Inmergers.Common.Utils;
using System.Threading.Tasks;

namespace Inmergers.Business.Service.TongSap
{
    public interface ITongSapServices
    {
        Task<Response> PBDangKyNhieuSanPhamNhat();
        Task<Response> SpDangKyNhieuNhat();
        Task<Response> ThangNamCoNhieuNhieuNhanVienNhat();
        Task<Response> ThangNamCoNhieuNhieuSanPhamDangKyNhat();

    }
}
