﻿using Inmergers.Business.Service.MatHang;
using Inmergers.Business.Service.PhieuDangKyMuaHang;
using Inmergers.Data.Data.Entity;
using System.Collections.Generic;

namespace Inmergers.Business.Service.TongSap
{
    public class ProductsByDepartment
    {
        public boPhan Department { get; set; }
        public int Total { get; set; }
        public List<Data.Data.Entity.MatHang> SupplyList { get; set; }
    }
    public class SPDangKySoLuongNhieuNhat
    {
        public string ten { get; set; }
        public int SoLuong { get; set; }
        public List<ChiTietPhieu> ChiTietPhieu { get; set; }
    }

    public class BoPhanLuu
    {
        public string TenBoPhanLuu { get; set; }
        public int SoLuong { get; set; }
    }

    public class NamNhieuSanPhamNhat
    {
        public int Nam { get; set; }
        public int Thang { get; set; }
        public int SoLuong { get; set; }
        public List<MatHangModel> MatHangModel { get; set; }
    }
    public class NamThangSoluongModels
    {
        public int nam { get; set; }
        public int thang { get; set; }
        public int soluong { get; set; }
    }
}
