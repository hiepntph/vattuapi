﻿using Inmergers.Business.Service.MatHang;
using Inmergers.Business.Service.PhieuDangKyMuaHang;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Inmergers.Business.Service.TongSap
{
    public class TongSapServices : ITongSapServices
    {
        private readonly vattudbContext _Context;

        public TongSapServices(vattudbContext context)
        {
            _Context = context;
        }

        public async Task<Response> PBDangKyNhieuSanPhamNhat()
        {
            try
            {
                var chitietphieug = _Context.ChiTietPhieuMuaHangs;
                var ListBoPhan = chitietphieug
                    .Include(ct => ct.PhieuDangKyMuaHang.NhanVien.boPhan)
                    .ToList()
                    .GroupBy(ct => ct.PhieuDangKyMuaHang.NhanVien.boPhan.tenBoPhan)
                    .ToDictionary(
                        g => g.Key,
                        g => g.Sum(ct => ct.SoLuong)
                    ).Select(p => new SPDangKySoLuongNhieuNhat
                    {
                        ten = p.Key,
                        SoLuong = p.Value,
                        ChiTietPhieu = chitietphieug
                            .Where(ct => ct.PhieuDangKyMuaHang.NhanVien.boPhan.tenBoPhan == p.Key)
                            .Select(s => new ChiTietPhieu()
                            {
                                soluongban = s.SoLuong
                            }).ToList()
                    })
                    .OrderByDescending(p => p.SoLuong)
                    .FirstOrDefault();

                return new ResponseObject<SPDangKySoLuongNhieuNhat>(ListBoPhan);
            }
            catch (Exception e)
            {
                Log.Error("Thống kê phòng ban đăng kí nhiều sản phẩm nhất không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + e.Message);
            }
        }

        public async Task<Response> SpDangKyNhieuNhat()
        {
            try
            {
                var ListSanPham = _Context.phieuDangKyMuaHangs
                    .Include(po => po.ChiTietPhieuMuaHang)
                    .ToList()
                    .GroupBy(po => po.ChiTietPhieuMuaHang)
                    .ToDictionary(
                        g => g.Key,
                        g => g.SelectMany(po => po.ChiTietPhieuMuaHang)
                            .GroupBy(s => _Context.matHangs.Find(s.IdMatHang).tenMatHang)
                            .ToDictionary(sg => sg.Key, sg => sg.Sum(s => s.SoLuong))
                    );
                var result = ListSanPham
                    .Select(p => new SPDangKySoLuongNhieuNhat
                    {
                        ten = p.Value.FirstOrDefault().Key,
                        SoLuong = p.Value.Sum(s => s.Value),
                        ChiTietPhieu = p.Value.Select(s => new ChiTietPhieu()
                        {
                            soluongban = s.Value
                        }).ToList()
                    })
                    .OrderByDescending(p => p.SoLuong)
                    .FirstOrDefault();

                return new ResponseObject<SPDangKySoLuongNhieuNhat>(result);
            }
            catch (Exception e)
            {
                Log.Error("Thống kê sản phẩm được đăng kí số lượng nhiều nhất không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + e.Message);
            }
        }

        public async Task<Response> ThangNamCoNhieuNhieuNhanVienNhat()
        {
            try
            {

                var DsNV = _Context.NhanViens
                    .GroupBy(x => new { x.ngaytao.Year, x.ngaytao.Month })
                    .Select(g => new
                    {
                        thang = g.Key.Month,
                        nam = g.Key.Year,
                        soluong = g.Count()
                    }).Select(c => new NamThangSoluongModels
                    {
                        nam = c.nam,
                        thang = c.thang,
                        soluong = c.soluong
                    })
                    .OrderByDescending(x => x.soluong)
                    .FirstOrDefault();

                return new ResponseObject<NamThangSoluongModels>(DsNV);
            }
            catch (Exception e)
            {
                Log.Error("Thống kê tháng năm nào có số lượng nhân viên mới nhiều không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + e.Message);
            }

        }

        public async Task<Response> ThangNamCoNhieuNhieuSanPhamDangKyNhat()
        {
            try
            {
                var ListSanPham = _Context.phieuDangKyMuaHangs
                    .Include(po => po.ChiTietPhieuMuaHang)
                    .ToList()
                    .GroupBy(po => $"{po.NgayTao.Year}-{po.NgayTao.Month}")
                    .ToDictionary(
                        g => g.Key,
                        g => g.SelectMany(po => po.ChiTietPhieuMuaHang)
                            .GroupBy(s => _Context.matHangs.Find(s.IdMatHang).tenMatHang)
                            .ToDictionary(sg => sg.Key, sg => sg.Sum(s => s.SoLuong))
                    );
                var result = ListSanPham
                    .Select(p => new NamNhieuSanPhamNhat
                    {
                        Thang = Convert.ToInt32(p.Key.Split('-')[1]),
                        Nam = Convert.ToInt32(p.Key.Split('-')[0]),
                        SoLuong = p.Value.Sum(s => s.Value),
                        MatHangModel = p.Value.Select(s => new MatHangModel()
                        {
                            tenMatHang = s.Key,
                            Tong = s.Value
                        }).ToList()
                    })
                    .OrderByDescending(p => p.SoLuong)
                    .FirstOrDefault();

                return new ResponseObject<NamNhieuSanPhamNhat>(result);
            }
            catch (Exception e)
            {
                Log.Error("Thống kê tháng năm nào có số lượng sản phẩm được đăng kí nhiều không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + e.Message);
            }
        }
    }
}

