﻿using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Inmergers.Business.Service.ThongKe
{
    public class ThongkeServices : IThongKeServices
    {

        private readonly vattudbContext _Context;

        public ThongkeServices(vattudbContext context)
        {
            _Context = context;
        }

        public async Task<Response> ThongKeSoLuongNhanVienTheoThang()
        {
            try
            {
                var result = await _Context.NhanViens
                    .GroupBy(x => new { x.ngaytao.Year, x.ngaytao.Month })
                    .Select(g => new SoLuongThangNam
                    {
                        nam = g.Key.Year,
                        thang = g.Key.Month,
                        soluong = g.Count()
                    })
                    .ToListAsync();
                return new ResponseList<SoLuongThangNam>(result);
            }
            catch (Exception e)
            {
                Log.Error("Thống kê số lượng nhân viên mới theo tháng không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + "Có lỗi trong quá trình xử lý: " + e.Message);
            }

        }


        public async Task<Response> ThongKeSoLuongNhanVienTrongBoPhan()
        {
            try
            {
                var result = await _Context.boPhans.Select(bophan => new SlNhanVienPhongBanModesl
                {
                    tenBoPhan = bophan.tenBoPhan,
                    soluong = bophan.nhanvien.Count
                }).ToListAsync();
                return new ResponseList<SlNhanVienPhongBanModesl>(result);
            }
            catch (Exception e)
            {
                Log.Error("Thống kê số lượng nhân viên theo phòng ban không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + "Có lỗi trong quá trình xử lý: " + e.Message);
            }

        }

        public async Task<Response> ThongKeSoLuongSanPhamDangkyTheoThang()
        {
            try
            {
                var result = await _Context.phieuDangKyMuaHangs
                    .GroupBy(x => new { x.NgayTao.Year, x.NgayTao.Month })
                    .Select(g => new SoLuongThangNam
                    {
                        nam = g.Key.Year,
                        thang = g.Key.Month,
                        soluong = g.Count()
                    })
                    .ToListAsync();
                return new ResponseList<SoLuongThangNam>(result);
            }
            catch (Exception e)
            {
                Log.Error("Thống kê số lượng đăng kí theo tháng không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + "Có lỗi trong quá trình xử lý: " + e.Message);
            }
        }
    }
}
