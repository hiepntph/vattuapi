﻿using System;
using System.Collections.Generic;
using System.Text;
using Inmergers.Data.Data.Entity;

namespace Inmergers.Business.Service.ThongKe
{
    public class SoLuongThangNam
    {
        public int nam { get; set; }
        public int thang { get; set; }
        public int soluong { get; set; }
    }
    public class SlNhanVienPhongBanModesl
    {
        public string tenBoPhan { get; set; }
        public int soluong { get; set; }
    }
}
