﻿using Inmergers.Common.Utils;
using System;

namespace Inmergers.Business.Service.MatHang
{
    public class MatHangModels
    {
        public Guid Id { get; set; }
        public string tenMatHang { get; set; }
        public int Stt { get; set; }
        public int soLuong { get; set; }
    }
    public class MatHangCreateAndUpdateModels
    {
        public string tenMatHang { get; set; }
        public int Stt { get; set; }
        public int soLuong { get; set; }
    }
    public class MatHangModel
    {
        public string tenMatHang { get; set; }
        public int Tong { get; set; }
    }
    public class MatHangQueryModel : PaginationRequest
    {
        public Guid? MatHangId { get; set; }
    }
}
