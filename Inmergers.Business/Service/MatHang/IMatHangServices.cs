﻿using Inmergers.Common.Utils;
using System;
using System.Threading.Tasks;

namespace Inmergers.Business.Service.MatHang
{
    public interface IMatHangServices
    {
        Task<Response> createMatHang(MatHangCreateAndUpdateModels model);
        Task<Response> updateMatHang(Guid Id, MatHangCreateAndUpdateModels model);
        Task<Response> getMatHang(MatHangQueryModel filter);
        Task<Response> getIdMatHang(Guid Id);
        Task<Response> deleteMatHang(Guid Id);
    }
}
